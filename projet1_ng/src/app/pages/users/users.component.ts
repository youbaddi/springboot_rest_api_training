import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/user.service";
import {User} from "../../common/User";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit{

  users: any =[];
  selectedId:number=0;
  constructor(private userservice : UserService) {
    this.getUsers();
  }
  ngOnInit(){
    this.getUsers();
  }

  getUsers(){
    this.userservice.getUsers().subscribe((data) => {
      console.log(data);
      this.users=data;
    })
  };

  setSelectedUser(num: number){
    console.log("set number slected user" + num);
    this.selectedId=num;
}

  deleteUser(){
    console.log("delete slected user" + this.selectedId);
    this.userservice.deleteUser(this.selectedId).subscribe(data => {
      // @ts-ignore
      console.log(data)
      // @ts-ignore
      console.log(data['status'])
      this.getUsers();
      // @ts-ignore
      $('#myModal').modal('hide')


    }, err => {
      console.log("error so navigate");
      // @ts-ignore
      console.log(err);
      // @ts-ignore
      // console.log(data['status'])
      this.rt.navigate(["/errorpage"]);
    });
  }

}
