import { Component } from '@angular/core';
import {FormControl, FormGroup, FormBuilder} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {User} from "../../common/User";
import {Router} from "@angular/router"

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent {
  userForm : FormGroup;


  constructor(private userservice:UserService,
              private fb:FormBuilder,
              private router: Router) {
    this.userForm = this.fb.group({
        nom: [''],
        prenom: [''],
      }

    );
  }

  addUser(data: any){
    var formData : any = new FormData();
    // @ts-ignore
    formData.append('nom', this.userForm.get('nom').value);
    // @ts-ignore
    formData.append('prenom', this.userForm.get('prenom').value);



    console.log("add user");
// @ts-ignore
    console.log(this.userForm.get('nom').value);

    // @ts-ignore
    let user = new User(this.userForm.get('nom').value, this.userForm.get('prenom').value);
    this.userservice.addUsers(user).subscribe(res => {
      console.log(res);
      this.router.navigate(["/users"]);
    })
  }
}
