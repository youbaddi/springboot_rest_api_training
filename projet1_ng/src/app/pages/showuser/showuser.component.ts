import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../services/user.service";
import {User} from "../../common/User";

@Component({
  selector: 'app-showuser',
  templateUrl: './showuser.component.html',
  styleUrls: ['./showuser.component.css']
})
export class ShowuserComponent implements OnInit{
  private id:number=0;
  private args:any;
  user:any;
  constructor(private route: ActivatedRoute,
              private rt : Router,
              private userService: UserService) {
  }
  ngOnInit(): void {
    this.args=this.route.params.subscribe(params => {
      this.id = params['id'];
      console.log(this.id);
      this.userService.showUser(this.id).subscribe(data => {
        // @ts-ignore
        console.log(data)
        // @ts-ignore
        console.log(data['status'])
        // @ts-ignore
        if(data['status']==200){
          this.user = data['body'];
        }

      }, err => {
        console.log("error so navigate");
        // @ts-ignore
        console.log(err);
        // @ts-ignore
        // console.log(data['status'])
        this.rt.navigate(["/errorpage"]);
      });
      }
    )
  }

}
