import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../common/User";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  backendUrl = 'http://localhost:8080'
  constructor(private http: HttpClient) { }

  getUsers(){
    return this.http.get(this.backendUrl+"/personnes");
  }

  addUsers(user:any){
    return this.http.post(this.backendUrl+"/personnes",user);
  }

  showUser(userId:any){
    console.log("service showuser");
    return this.http.get(this.backendUrl+"/personnes/"+userId, { observe: 'response' });
  }

  deleteUser(userId:any){
    console.log("service showuser");
    return this.http.delete(this.backendUrl+"/personnes/"+userId, { observe: 'response' });
  }
}
