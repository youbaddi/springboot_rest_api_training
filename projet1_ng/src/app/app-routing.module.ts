import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {UsersComponent} from "./pages/users/users.component";
import {DashboardComponent} from "./pages/dashboard/dashboard.component";
import {AdduserComponent} from "./pages/adduser/adduser.component";
import {ShowuserComponent} from "./pages/showuser/showuser.component";
import {ErrorpageComponent} from "./pages/errorpage/errorpage.component";

const routes: Routes = [
  {path:'', component: DashboardComponent},
  // {path:'/', component: DashboardComponent},
  {path:'users', component: UsersComponent},
  {path:'users/show/:id', component: ShowuserComponent},
  {path:'users/edit/:id', component: ShowuserComponent},
  {path:'users/add', component: AdduserComponent},
  {path:'errorpage', component: ErrorpageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
