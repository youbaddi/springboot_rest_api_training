import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppheaderComponent } from './components/appheader/appheader.component';
import { AppmenuComponent } from './components/appmenu/appmenu.component';
import { AppfooterComponent } from './components/appfooter/appfooter.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { UsersComponent } from './pages/users/users.component';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import { AdduserComponent } from './pages/adduser/adduser.component';
import {FormGroupDirective, FormsModule, ReactiveFormsModule} from "@angular/forms";
import { EdituserComponent } from './pages/edituser/edituser.component';
import { ShowuserComponent } from './pages/showuser/showuser.component';
import { ErrorpageComponent } from './pages/errorpage/errorpage.component';

@NgModule({
  declarations: [
    AppComponent,
    AppheaderComponent,
    AppmenuComponent,
    AppfooterComponent,
    DashboardComponent,
    UsersComponent,
    AdduserComponent,
    EdituserComponent,
    ShowuserComponent,
    ErrorpageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
