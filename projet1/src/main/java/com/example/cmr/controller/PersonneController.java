package com.example.cmr.controller;

import java.util.List;

import com.example.cmr.dao.AdresseRepository;
import com.example.cmr.dao.PersonneRepository;
import com.example.cmr.exception.PersonneNotFoundException;
import com.example.cmr.model.Personne;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin()
@RestController
public class PersonneController {

	@Autowired
	private PersonneRepository pr;
	
	@Autowired
	private AdresseRepository ar;
	
//	TODO getPersoones
	@GetMapping(value="/personnes", 
			produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public List<Personne> getPersoones(){
		pr.findAll().stream().forEach(System.out::println);
		return pr.findAll();
	}
	
	// TODO
	@GetMapping("/personnes/{id}")
	public ResponseEntity<Personne> getPersonnes(@PathVariable("id") long id){
		Personne personne = pr.findById(id).orElse(null);
		if(personne == null) {
//			throw new PersonneNotFoundException(); 
			return new ResponseEntity<Personne>(new Personne(), HttpStatus.BAD_REQUEST);
		}
		
		ResponseEntity<Personne> re = new ResponseEntity<Personne>(personne, HttpStatus.OK);
		return re ;
	}
	
//	 TODO
	@PostMapping("/personnes")
	public Personne addPersonne(@RequestBody Personne personne){
		System.out.println(personne);
//		ar.saveAll(personne.getAdresses());
		
		return pr.saveAndFlush(personne);
		
	}
	
//	TODO del function
	@DeleteMapping("/personnes/{id}")
	public ResponseEntity<Boolean> deletePersonne(@PathVariable("id") long id){
		Personne personne = pr.findById(id).orElse(null);
		if(personne == null) {
//			throw new PersonneNotFoundException(); 
			return new ResponseEntity<Boolean>(true, HttpStatus.BAD_REQUEST);
		}
		
		pr.deleteById(id);
		ResponseEntity<Boolean> re = new ResponseEntity<Boolean>(true, HttpStatus.OK);
		return re ;
		
	}
	
}
