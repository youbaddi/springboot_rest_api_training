package com.example.cmr;

import com.example.cmr.dao.PersonneRepository;
import com.example.cmr.model.Personne;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Projet1Application implements ApplicationRunner{

	@Autowired
	private PersonneRepository pr;
	
	public static void main(String[] args) {
		SpringApplication.run(Projet1Application.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// TODO Auto-generated method stub
//		Personne p1 = new Personne("baddi", "youssef");
//		Personne p2 = new Personne("Ahmas", "Yassine");
//		pr.save(p1);
//		pr.save(p2);
	}

}
