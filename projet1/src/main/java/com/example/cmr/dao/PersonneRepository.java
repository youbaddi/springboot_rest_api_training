package com.example.cmr.dao;

import java.util.List;

import com.example.cmr.model.Personne;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonneRepository extends JpaRepository<Personne, Long>{
public List<Personne> findByNom(String nom);
}
