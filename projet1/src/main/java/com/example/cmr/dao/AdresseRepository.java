package com.example.cmr.dao;

import java.util.List;

import com.example.cmr.model.Adresse;
import com.example.cmr.model.Personne;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AdresseRepository extends JpaRepository<Adresse, Long>{

}
