package com.example.cmr.model;

import java.util.List;

import org.hibernate.annotations.ManyToAny;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="etudiant")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
public class Personne {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
private Long num;
	@NonNull
private String nom;
	@NonNull
private String prenom;
	
//	@NonNull
	@ManyToMany(cascade ={ CascadeType.MERGE, CascadeType.REFRESH},fetch = FetchType.EAGER)
	private List<Adresse> adresses;
	
}
